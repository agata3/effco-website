/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';
import { ReactComponent as Minimal } from './minimal.svg';
import { ReactComponent as Loading } from './loading.svg';
import { ReactComponent as Posibilities } from './posibilities.svg';
import { ReactComponent as Design } from './design.svg';
const mock = [
  {
    title: 'Minimal Design',
    subtitle:
      'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    icon: (
      <Minimal />
      // <svg
      //   height={24}
      //   width={24}
      //   xmlns="http://www.w3.org/2000/svg"
      //   fill="none"
      //   viewBox="0 0 24 24"
      //   stroke="currentColor"
      // >
      //   <path
      //     strokeLinecap="round"
      //     strokeLinejoin="round"
      //     strokeWidth={2}
      //     d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"
      //   />
      // </svg>
    ),
  },
  {
    title: 'Fast Loading',
    subtitle:
      'Designed with the latest design trends in mind. theFront feels modern, minimal, and beautiful.',
    icon: (
      <Loading />
      // <svg
      //   height={24}
      //   width={24}
      //   xmlns="http://www.w3.org/2000/svg"
      //   fill="none"
      //   viewBox="0 0 24 24"
      //   stroke="currentColor"
      // >
      //   <path
      //     strokeLinecap="round"
      //     strokeLinejoin="round"
      //     strokeWidth={2}
      //     d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"
      //   />
      // </svg>
    ),
  },
  {
    title: 'Unlimited Posibilities',
    subtitle:
      'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    icon: (
      <Posibilities />
      // <svg
      //   height={24}
      //   width={24}
      //   xmlns="http://www.w3.org/2000/svg"
      //   fill="none"
      //   viewBox="0 0 24 24"
      //   stroke="currentColor"
      // >
      //   <path
      //     strokeLinecap="round"
      //     strokeLinejoin="round"
      //     strokeWidth={2}
      //     d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4"
      //   />
      // </svg>
    ),
  },
  {
    title: 'Component Based Design',
    subtitle:
      ' Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    icon: (
      <Design />
      // <svg
      //   height={24}
      //   width={24}
      //   xmlns="http://www.w3.org/2000/svg"
      //   fill="none"
      //   viewBox="0 0 24 24"
      //   stroke="currentColor"
      // >
      //   <path
      //     strokeLinecap="round"
      //     strokeLinejoin="round"
      //     strokeWidth={2}
      //     d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4"
      //   />
      // </svg>
    ),
  },
];

const Services = (): JSX.Element => {
  const theme = useTheme();
  return (
    <Grid container>
      <Grid item xs={12} md={8}>
        <Box marginTop={40} marginBottom={40}>
          <Box>
            <Box>
              <Typography
                variant="h4"
                color="text.primary"
                align={'left'}
                gutterBottom
                sx={{
                  fontWeight: 700,
                }}
              >
                Our Portfolio
              </Typography>
              <Typography
                marginRight={20}
                variant="h6"
                component="p"
                color="text.secondary"
                sx={{ fontWeight: 400 }}
                align={'left'}
              >
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia.
              </Typography>
            </Box>
          </Box>
          <Grid container spacing={2} marginTop={5}>
            {mock.map((item, i) => (
              <Grid item xs={12} md={6} key={i}>
                <Box
                  width={1}
                  height={1}
                  data-aos={'fade-up'}
                  data-aos-delay={i * 100}
                  data-aos-offset={100}
                  data-aos-duration={600}
                  marginBottom={4}
                >
                  <Box
                    display={'flex'}
                    flexDirection={'column'}
                    alignItems={'left'}
                  >
                    <Box
                      component={Avatar}
                      width={60}
                      height={60}
                      marginBottom={2}
                      bgcolor={alpha(theme.palette.primary.main, 0.1)}
                      color={theme.palette.primary.main}
                    >
                      {item.icon}
                    </Box>
                    <Typography
                      variant={'h6'}
                      gutterBottom
                      sx={{ fontWeight: 500 }}
                      align={'left'}
                    >
                      {item.title}
                    </Typography>
                    <Typography
                      align={'left'}
                      color="text.secondary"
                      marginRight={12}
                    >
                      {item.subtitle}
                    </Typography>
                  </Box>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Grid>
    </Grid>
  );
};

export default Services;
