import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useTheme } from '@mui/material/styles';

const mock = [
  {
    title: 'Who are you?',
    text: 'We are a group of highly motivated individuals; passionate about crafting software that helps businesses generate more revenue, cut down cost, improve quality and letting workforce focus not on menial tasks but creating value for their customers and in turn the companies.',
  },
  {
    title: 'Where are you based?',
    text: ' Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat!',
  },
  {
    title: 'How quickly can I start benefiting?',
    text: ' Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat!',
  },
  {
    title: 'How much do the Instant App cost?',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt rerum minima a possimus, amet perferendis, temporibus obcaecati pariatur. Reprehenderit magnam necessitatibus vel culpa provident quas nesciunt sunt aut cupiditate fugiat!',
  },
  {
    title: 'I have technical problem, who do I email?',
    text: 'On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country.',
  },
];

const Questions = (): JSX.Element => {
  const theme = useTheme();
  return (
    <Box>
      <Box>
        <Box marginBottom={2}>
          <Typography
            variant="h6"
            align={'center'}
            sx={{
              color: theme.palette.common.black,
              fontWeight: 400,
            }}
          >
            FAQ
          </Typography>
        </Box>
        <Box>
          <Typography
            variant="h4"
            align={'center'}
            sx={{
              color: theme.palette.common.black,
              fontWeight: 700,
            }}
          >
            Frequently Asked Questions
          </Typography>
        </Box>
      </Box>
      <Box marginTop={10}>
        {mock.map((item, i) => (
          <Box
            component={Accordion}
            key={i}
            paddingY={2}
            elevation={0}
            sx={{
              '&:first-of-type': {
                borderTopLeftRadius: 1,
                borderTopRightRadius: 1,
              },
              '&:not(:first-of-type):before': {
                opacity: '1 !important',
                display: 'block !important',
              },
              '&.Mui-expanded': {
                margin: 0,
              },
            }}
          >
            <Box
              component={AccordionSummary}
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id={`panel1a-header--${i}`}
              padding={'0 !important'}
            >
              <Box>
                <Typography fontWeight={600} fontSize={18}>
                  {item.title}
                </Typography>
              </Box>
            </Box>
            <AccordionDetails>
              <Typography>{item.text}</Typography>
            </AccordionDetails>
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default Questions;
