import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';

import Container from 'components/Container';

const Hero = (): JSX.Element => {
  const theme = useTheme();
  return (
    <Box>
      <Container
        zIndex={3}
        position={'relative'}
        minHeight={{ xs: 300, sm: 400, md: 600 }}
        maxHeight={600}
        display={'flex'}
        alignItems={'center'}
        justifyContent={'center'}
      >
        <Box>
          <Box marginBottom={2}>
            <Typography
              variant="h6"
              align={'center'}
              sx={{
                color: theme.palette.common.black,
                fontWeight: 700,
              }}
            >
              WHAT WE DO
            </Typography>
          </Box>
          <Box>
            <Typography
              variant="h4"
              align={'center'}
              sx={{
                color: theme.palette.common.black,
                fontWeight: 700,
              }}
            >
              Our solutions
            </Typography>
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default Hero;
