/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { Form } from 'views/PasswordResetSimple/components';

const Teaser = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  return (
    <section>
      <Box paddingTop={15}>
        <Grid container spacing={4} direction={isMd ? 'row' : 'column'}>
          <Grid
            item
            container
            alignItems={'center'}
            xs={12}
            md={6}
            data-aos={isMd ? 'fade-right' : 'fade-up'}
          >
            <Box>
              <Typography
                variant={'h3'}
                gutterBottom
                sx={{ fontWeight: 700, color: theme.palette.common.black }}
              >
                We deliver solutions
              </Typography>
              <Typography
                variant={'h6'}
                component={'p'}
                color={'text.secondary'}
              >
                Our software transformed operations of businesses of various
                sizes & many industries... One line of code at a time.
              </Typography>
            </Box>
          </Grid>
          <Grid
            item
            container
            justifyContent="center"
            alignItems="center"
            xs={12}
            md={6}
          >
            <Box maxWidth={490} width={1}>
              <Form />
            </Box>
          </Grid>
        </Grid>
      </Box>
    </section>
  );
};

export default Teaser;
