import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { alpha, useTheme } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/Menu';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { NavItem } from './components';
import Logo from './Logo.png';
interface Props {
  // eslint-disable-next-line @typescript-eslint/ban-types
  onSidebarOpen: () => void;
  pages: {
    landings: Array<PageItem>;
    company: Array<PageItem>;
    account: Array<PageItem>;
    secondary: Array<PageItem>;
    blog: Array<PageItem>;
    portfolio: Array<PageItem>;
  };
  colorInvert?: boolean;
}

const Topbar = ({
  onSidebarOpen,
  pages,
  colorInvert = true,
}: Props): JSX.Element => {
  const theme = useTheme();
  // const { mode } = theme.palette;
  const {
    landings: landingPages,
    secondary: secondaryPages,

    portfolio: portfolioPages,
    blog: blogPages,
  } = pages;

  return (
    <Box
      display={'flex'}
      justifyContent={'space-between'}
      alignItems={'center'}
      width={1}
    >
      {/* <Box
        display={'flex'}
        component="a"
        href="/"
        title="theFront"
        width={{ xs: 100, md: 120 }}
      > */}
      {/* <Box
          component={'img'}
          src={
            mode === 'light' && !colorInvert
              ? 'https://assets.maccarianagency.com/the-front/logos/logo.svg'
              : 'https://assets.maccarianagency.com/the-front/logos/logo-negative.svg'
          }
          height={1}
          width={1}
        /> */}
      {/* </Box> */}
      <Box width={{ xs: 100, md: 120 }}>
        <img src={Logo} height={'40'} />
      </Box>
      <Box sx={{ display: { xs: 'none', md: 'flex' } }} alignItems={'center'}>
        <Box>
          <AnchorLink
            href="#home"
            style={{ textDecoration: 'none', color: '#143157' }}
          >
            <NavItem
              title={'Home'}
              id={'landing-pages'}
              items={landingPages}
              colorInvert={colorInvert}
            />
          </AnchorLink>
        </Box>
        {/* <Box marginLeft={4}>
          <NavItem
            title={'Company'}
            id={'company-pages'}
            items={companyPages}
            colorInvert={colorInvert}
          />
        </Box>
        <Box marginLeft={4}>
          <NavItem
            title={'Account'}
            id={'account-pages'}
            items={accountPages}
            colorInvert={colorInvert}
          />
        </Box> */}
        <Box marginLeft={4}>
          <AnchorLink
            href="#us"
            style={{ textDecoration: 'none', color: '#143157' }}
          >
            <NavItem
              title={'What We Do'}
              id={'secondary-pages'}
              items={secondaryPages}
              colorInvert={colorInvert}
            />
          </AnchorLink>
        </Box>
        <Box marginLeft={4}>
          <AnchorLink
            href="#portfolio"
            style={{ textDecoration: 'none', color: '#143157' }}
          >
            <NavItem
              title={'Portfolio'}
              id={'blog-pages'}
              items={blogPages}
              colorInvert={colorInvert}
            />
          </AnchorLink>
        </Box>
        <Box marginLeft={4}>
          <AnchorLink
            href="#faq"
            style={{ textDecoration: 'none', color: '#143157' }}
          >
            <NavItem
              title={'FAQ'}
              id={'portfolio-pages'}
              items={portfolioPages}
              colorInvert={colorInvert}
            />
          </AnchorLink>
        </Box>
        <Box marginLeft={4}>
          <AnchorLink
            href="#button"
            style={{ textDecoration: 'none', color: '#143157' }}
          >
            <Button
              variant="contained"
              color="primary"
              component="a"
              target="blank"
              // href="https://mui.com/store/items/the-front-landing-page/"
              size="large"
            >
              Get Started
            </Button>
          </AnchorLink>
        </Box>
      </Box>
      <Box sx={{ display: { xs: 'flex', md: 'none' } }} alignItems={'center'}>
        <Button
          onClick={() => onSidebarOpen()}
          aria-label="Menu"
          variant={'outlined'}
          sx={{
            borderRadius: 2,
            minWidth: 'auto',
            padding: 1,
            borderColor: alpha(theme.palette.divider, 0.2),
          }}
        >
          <MenuIcon />
        </Button>
      </Box>
    </Box>
  );
};

export default Topbar;
