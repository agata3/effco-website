import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
// import { useTheme } from '@mui/material/styles';
import Logo from './Logo.png';
import NavItem from './components/NavItem';
import AnchorLink from 'react-anchor-link-smooth-scroll';
interface Props {
  pages: {
    landings: Array<PageItem>;
    company: Array<PageItem>;
    account: Array<PageItem>;
    secondary: Array<PageItem>;
    blog: Array<PageItem>;
    portfolio: Array<PageItem>;
  };
}

const SidebarNav = ({ pages }: Props): JSX.Element => {
  // const theme = useTheme();
  // const { mode } = theme.palette;

  const {
    landings: landingPages,
    secondary: secondaryPages,
    // company: companyPages,
    // account: accountPages,
    portfolio: portfolioPages,
    blog: blogPages,
  } = pages;

  return (
    <Box>
      <Box width={1} paddingX={2} paddingY={1}>
        {/* <Box
          display={'flex'}
          component="a"
          href="/"
          title="theFront"
          width={{ xs: 100, md: 120 }}
        >
        <Box
        component={'img'}
        src={
          mode === 'light'
          ? 'https://assets.maccarianagency.com/the-front/logos/logo.svg'
          : 'https://assets.maccarianagency.com/the-front/logos/logo-negative.svg'
        }
        height={1}
        width={1}
        />
      </Box> */}
        <Box>
          <img src={Logo} height={'40'} />
        </Box>
      </Box>
      <Box paddingX={2} paddingY={2}>
        <Box marginY={2}>
          <AnchorLink href="#home" style={{ textDecoration: 'none' }}>
            <NavItem title={'Home'} id={'landing-pages'} items={landingPages} />
          </AnchorLink>
        </Box>
        <Box marginY={2}>
          <AnchorLink href="#us" style={{ textDecoration: 'none' }}>
            <NavItem
              title={'What We Do'}
              id={'secondary-pages'}
              items={secondaryPages}
            />
          </AnchorLink>
        </Box>
        <Box marginY={2}>
          <AnchorLink href="#portfolio" style={{ textDecoration: 'none' }}>
            <NavItem title={'Portfolio'} id={'blog-pages'} items={blogPages} />
          </AnchorLink>
        </Box>
        <Box marginY={2}>
          <AnchorLink href="#faq" style={{ textDecoration: 'none' }}>
            <NavItem
              title={'FAQ'}
              id={'portfolio-pages'}
              items={portfolioPages}
            />
          </AnchorLink>
        </Box>
        {/* <Box>
          <NavItem title={'Blog'} items={[]} />
        </Box>
        <Box>
          <NavItem title={'Portfolio'} items={[]} />
        </Box> */}
        <Box marginTop={2} marginY={2}>
          <AnchorLink href="#button" style={{ textDecoration: 'none' }}>
            <Button
              size={'large'}
              variant="outlined"
              fullWidth
              component="a"
              // href="/docs/introduction"
            >
              Get Started
            </Button>
          </AnchorLink>
        </Box>
        {/* <Box marginTop={1}>
          <Button
            size={'large'}
            variant="contained"
            color="primary"
            fullWidth
            component="a"
            target="blank"
            href="https://mui.com/store/items/the-front-landing-page/"
          >
            Purchase now
          </Button>
        </Box> */}
      </Box>
    </Box>
  );
};

export default SidebarNav;
