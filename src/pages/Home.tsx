import React from 'react';
import { Main } from 'layouts';
import { Teaser } from 'views/Rental/components';
import Container from 'components/Container';
import { Features } from 'views/Startup/components';
import { Questions } from 'views/HelpCenterArticle/components';
import { Services } from 'views/IndexView/components';
import { Jumbo } from 'components/Jumbo';

export const Home = (): JSX.Element => {
  return (
    <Main colorInvert={true}>
      {/* <Box bgcolor={'linear-gradient(45deg, #5433ff 0%, #20bdff 100%)'}></Box> */}
      <section id="home">
        <Jumbo />
      </section>
      <Container paddingY={4}>
        {/* <Teaser />  */}
        <section id="us">
          <Features />
        </section>
        <section id="portfolio">
          <Services />
        </section>

        <section id="faq">
          <Questions />
        </section>
        <section id="button">
          <Teaser />
        </section>
      </Container>
    </Main>
  );
};
