import { Theme } from '@mui/material';
import { Box, SystemStyleObject } from '@mui/system';
import React from 'react';
import { Teaser } from 'views/Rental/components';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface JumboProps {}

export const Jumbo: React.FC<JumboProps> = () => {
  return (
    <Box
      bgcolor="#f2f4f9"
      sx={{
        ':after': {
          content: '""',
          position: 'absolute',
          bottom: '-100px',
          left: '-50px',
          height: '200px',
          width: '100%',
          backgroundColor: '#f2f4f9',
          WebkitTransform: 'rotate(-4deg)',
          MsTransform: 'rotate(-4deg)' as SystemStyleObject<Theme>,
          transform: 'rotate(-4deg)',
          zIndex: '10',
        },
        height: '95vh',
      }}
    >
      <Teaser />
    </Box>
  );
};
